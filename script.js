/*activity*/

/*1.*/
  let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
}

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"],
}

const introduce = (student) => {

	//Note: You can destructure objects inside functions.

	console.log(`Hi! I'm ${student.name}. I am ${student.age} years old.`);
	console.log(`I study the following courses ${student.classes}.`);
}

introduce(student1);
introduce(student2);

const getCube = (num) => Math.pow(num,3);
let cube = getCube(3);
console.log(cube);


let numArr = [15,16,32,21,21,2]

numArr.forEach(num => 
	console.log(num));

let numSquared = numArr.map(num => 
	console.log(Math.pow(num,2)))

/*2.*/

class dog {
	constructor(name, breed, dogAge){
		this.name = name;
		this.breed = breed;
		this.dogAge = dogAge * 7;
	}
}
 /*3.*/

let dog1 = new dog('mingming', 'askal', 4)
let dog2 = new dog('corcor', 'corgi', 5)


console.log(dog1);
console.log(dog2);




